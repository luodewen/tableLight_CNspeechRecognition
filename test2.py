#单纯录个5s的音
import pyaudio
import wave
in_path ="input.wav"
def save_audio(p,frames):
    wf = wave.open(in_path, 'wb')
    wf.setnchannels(1)#声道数1
    wf.setsampwidth(p.get_sample_size(pyaudio.paInt16))
    wf.setframerate(11025)
    wf.writeframes(b''.join(frames))
    wf.close()
def get_audio(filepath):
    CHUNK = 256
    FORMAT = pyaudio.paInt16
    RATE = 11025
    RECORD_SECONDS = 5#录音时间
    WAVE_OUTPUT_FILENAME = filepath
    p = pyaudio.PyAudio()

    stream = p.open(format=FORMAT,
                    channels=1,
                    rate=RATE,
                    input=True,
                    frames_per_buffer=CHUNK)

    frames = []
    while True:
        for i in range(0, int(RATE / CHUNK * RECORD_SECONDS)):
            data = stream.read(CHUNK)
            frames.append(data)
        print("end")

        # stream.stop_stream()
        # stream.close()
        # p.terminate()
        save_audio(p,frames)
get_audio(in_path)

import pygame as pg
def get_stick():
    return pg.transform.scale(pg.image.load('./asset/stick.png'),(250,250))
rotation=-50
size=200
def get_lights():
    return {
            'r':[
                pg.transform.rotate(pg.transform.scale(pg.image.load('./asset/r1.png'),(size,size)),rotation),
                pg.transform.rotate(pg.transform.scale(pg.image.load('./asset/r2.png'),(size,size)),rotation),
                pg.transform.rotate(pg.transform.scale(pg.image.load('./asset/r3.png'),(size,size)),rotation)
            ],
            'g':[
                pg.transform.rotate(pg.transform.scale(pg.image.load('./asset/g1.png'),(size,size)),rotation),
                pg.transform.rotate(pg.transform.scale(pg.image.load('./asset/g2.png'),(size,size)),rotation),
                pg.transform.rotate(pg.transform.scale(pg.image.load('./asset/g3.png'),(size,size)),rotation)
            ],
            'y':[
                pg.transform.rotate(pg.transform.scale(pg.image.load('./asset/y1.png'),(size,size)),rotation),
                pg.transform.rotate(pg.transform.scale(pg.image.load('./asset/y2.png'),(size,size)),rotation),
                pg.transform.rotate(pg.transform.scale(pg.image.load('./asset/y3.png'),(size,size)),rotation)
            ],

            }


